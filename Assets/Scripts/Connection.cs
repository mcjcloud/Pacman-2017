﻿using UnityEngine;
using System.Collections;

public class Connection {
    public Waypoint w1;
    public Waypoint w2;
    public readonly string ConnectionString = "";

    public Connection(Waypoint w1, Waypoint w2)
    {
        this.w1 = w1;
        this.w2 = w2;

        var w1Pos = w1.gameObject.transform.position;
        var w2Pos = w2.gameObject.transform.position;
        ConnectionString = "" + w1Pos.x + w1Pos.y + w2Pos.x + w2Pos.y;
    }
}
