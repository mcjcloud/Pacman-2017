﻿using UnityEngine;
using System.Collections;
using System;

public class Waypoint : MonoBehaviour {

    public enum Direction { Up = 2, Down = -2, Left = -1, Right = 1 };

    // properties
    public Direction[] PossibleDirections;

    /*
     * Static functions
     */
    public static Waypoint getClosestWaypoint(Waypoint current, Waypoint.Direction dir, Waypoint[] waypoints)
    {
        var currentPos = current.transform.position;

        Waypoint closest = null;
        foreach (var waypoint in waypoints)
        {
            var pos = waypoint.transform.position;
            // determine what axis to look on
            switch (dir)
            {
                case Waypoint.Direction.Left:
                    // if: the waypoint is to the left, the y axis is the same, and its closer than the current closest
                    if (pos.x < currentPos.x && pos.y == currentPos.y && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Left) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Left))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Right) && canGoDirection(current, Waypoint.Direction.Left))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Right) && canGoDirection(current, Waypoint.Direction.Left))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Right:
                    // if: the waypoint is to the right, the y axis is the same, and its closer than the current closest
                    if (pos.x > currentPos.x && pos.y == currentPos.y && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Right) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Right))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Left) && canGoDirection(current, Waypoint.Direction.Right))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Left) && canGoDirection(current, Waypoint.Direction.Right))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Down:
                    // if: the waypoint is down from, the x axis is the same, and its closer than the current closest
                    if (pos.y < currentPos.y && pos.x == currentPos.x && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Down) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Down))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Up) && canGoDirection(current, Waypoint.Direction.Down))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Up) && canGoDirection(current, Waypoint.Direction.Down))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Up:
                    // if: the waypoint is down from, the x axis is the same, and its closer than the current closest
                    if (pos.y > currentPos.y && pos.x == currentPos.x && waypoint != current)
                    {
                        //Debug.Log("case UP; x: " + currentPos.x + " y: " + currentPos.y + " x(1): " + pos.x + "y(1): " + pos.y);
                        Debug.Log("fulfills requirements");
                        if (closest != null)
                        {
                            //Debug.Log("closest != null");
                            //Debug.Log("closest dist: " + getDistance(currentPos, pos, Waypoint.Direction.Up));
                            if (getDistance(currentPos, pos, Waypoint.Direction.Up) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Up))
                            {
                                Debug.Log("closer");
                                if (canGoDirection(waypoint, Waypoint.Direction.Down) && canGoDirection(current, Waypoint.Direction.Up))
                                {
                                    Debug.Log("closer and can go");
                                    closest = waypoint;
                                }
                            }
                            else
                            {
                                Debug.Log("no closer");
                            }
                        }
                        else
                        {
                            Debug.Log("closest == null");
                            if (canGoDirection(waypoint, Waypoint.Direction.Down) && canGoDirection(current, Waypoint.Direction.Up))
                            {
                                Debug.Log("closest null, can go direction");
                                closest = waypoint;
                            }
                        }
                    }
                    break;
            }
        }

        // return the closest waypoint
        return closest;
    }

    // gets the distance between vectors in a direction
    public static double getDistance(Vector3 v1, Vector3 v2, Waypoint.Direction dir)
    {
        switch (dir)
        {
            case Waypoint.Direction.Left:
                return Math.Abs(v2.x - v1.x);
            case Waypoint.Direction.Right:
                return Math.Abs(v2.x - v1.x);
            case Waypoint.Direction.Down:
                return Math.Abs(v2.y - v1.y);
            case Waypoint.Direction.Up:
                return Math.Abs(v2.y - v1.y);
        }
        return 0.0;
    }

    // see if a waypoint has a certain direction.
    public static bool canGoDirection(Waypoint waypoint, Waypoint.Direction direction)
    {
        foreach (var dir in waypoint.PossibleDirections)
        {
            if (dir == direction) return true;
        }
        return false;
    }

    public override string ToString()
    {
        return transform.position + "";
    }
}
