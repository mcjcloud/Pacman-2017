﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Text ScoreLabel;
    public float DotSpacing = 0.5f;
    public GameObject Dot;
    public GameObject ConnectionsParent;
    public GameObject WaypointsParent;
    private Waypoint[] Waypoints;
    private Connection[] Connections;

    private static int DotScore = 10;
    private static int score = 0;

	// Use this for initialization
	void Start () {
        Waypoints = new Waypoint[WaypointsParent.transform.childCount];
        int i = 0;
        foreach(Transform tform in WaypointsParent.transform)
        {
            Waypoints[i++] = tform.gameObject.GetComponent<Waypoint>();
        }

        // solve for connections array
        ArrayList connections = new ArrayList();
        foreach(Waypoint wp in Waypoints)
        {
            // determine the possible directions
            bool left = false, right = false, down = false, up = false;
            foreach(var dir in wp.PossibleDirections)
            {
                if (dir == Waypoint.Direction.Left) left = true;
                if (dir == Waypoint.Direction.Right) right = true;
                if (dir == Waypoint.Direction.Down) down = true;
                if (dir == Waypoint.Direction.Up) up = true;
            }

            if (left ^ right)        // left xor right OR up xor down
            {
                Waypoint p2 = null;
                if(left)
                {
                    // find the waypoint furthest left until you get a wall.
                    p2 = furthestFromPoint(wp, Waypoint.Direction.Left);
                }
                else if (right)
                {
                    // find the waypoint furthest right until you get a wall.
                    p2 = furthestFromPoint(wp, Waypoint.Direction.Right);
                }

                // build the connection
                if (wp != null && p2 != null)
                {
                    Connection con = new Connection(wp, p2);
                    // check that it isn't contained.
                    bool conHas = false;
                    foreach (Connection _con in connections)
                    {
                        if ((_con.w1 == con.w1 && _con.w2 == con.w2) || (_con.w2 == con.w1 && _con.w1 == con.w2))
                        {
                            conHas = true;
                            break;
                        }
                    }
                    if (!conHas)
                    {
                        connections.Add(con);
                    }
                }
            }
            if (up ^ down)
            {
                Waypoint p2 = null;
                if (up)
                {
                    // find the waypoint furthest up until you get a wall.
                    p2 = furthestFromPoint(wp, Waypoint.Direction.Up);
                }
                else if (down)
                {
                    // find the waypoint furthest down until you get a wall.
                    p2 = furthestFromPoint(wp, Waypoint.Direction.Down);
                }

                // build the connection
                if (wp != null && p2 != null)
                {
                    Connection con = new Connection(wp, p2);
                    // check that it isn't contained.
                    bool conHas = false;
                    foreach (Connection _con in connections)
                    {
                        if ((_con.w1 == con.w1 && _con.w2 == con.w2) || (_con.w2 == con.w1 && _con.w1 == con.w2))
                        {
                            conHas = true;
                            break;
                        }
                    }
                    if (!conHas)
                    {
                        connections.Add(con);
                    }
                }
            }
            else
            {
                // either you can go in both directions, or none, so this isn't a relavent waypoint.
            }
        }

        // insert the dots
        foreach(Connection con in connections)
        {
            var pos1 = con.w1.transform.position;
            var pos2 = con.w2.transform.position;
            float distance = Mathf.Sqrt(Mathf.Pow(pos2.y - pos1.y, 2) + Mathf.Pow(pos2.x - pos1.x, 2));
            int waypointCount = (int) (distance / DotSpacing);

            float xIncScale = (pos2.x - pos1.x) / distance;
            float yIncScale = (pos2.y - pos1.y) / distance;

            for(int j = 0; j <= waypointCount; j++)
            {
                float x = pos1.x + (j * (xIncScale * DotSpacing));
                float y = pos1.y + (j * (yIncScale * DotSpacing));

                var dot = Instantiate(Dot);
                dot.transform.position = new Vector3(x, y, dot.transform.position.z);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        ScoreLabel.text = "" + score;
	}

    public static void CollectedDot()
    {
        score += DotScore;
    }

    
    // HELPER
    private Waypoint furthestFromPoint(Waypoint point, Waypoint.Direction dir)
    {
        Waypoint next = point;
        bool cont = true;
        // get the next waypoint
        do
        {
            next = Waypoint.getClosestWaypoint(next, dir, Waypoints);

            cont = false;
            if (next != null)
            {
                foreach (var _dir in next.PossibleDirections)
                {
                    if (_dir == dir)
                    {
                        cont = true;
                    }
                }
            }
        } while (cont);

        return next;
    }


}
