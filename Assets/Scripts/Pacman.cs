﻿using UnityEngine;
using System.Collections;
using System;
//using static Waypoint;

public class Pacman : MonoBehaviour
{

    public GameObject WaypointParent;                   // teh parent game object containing all of the waypoints
    private Waypoint[] Waypoints;                        // an array of the waypoints on the map.

    public Waypoint CurrentWaypoint;                    // the waypoint our player is currently moving to.
    public Waypoint rightTeleport;
    public Waypoint leftTeleport;
    public Waypoint.Direction Direction;                // 0 = right, 1 = down, 2 = left, 3 = up
    public float speed;
    public float turnForgiveness;                       // the amount of distance before an intersection you can turn at

    private Animator anim;

    // Use this for initialization
    void Start()
    {
        Waypoints = new Waypoint[WaypointParent.transform.childCount];
        int i = 0;
        foreach (Transform tform in WaypointParent.transform)
        {
            Waypoints[i] = tform.gameObject.GetComponent<Waypoint>();
            i++;
        }
        Debug.Log(Waypoints);

        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("dot"))
        {
            if (col.gameObject != null)
            {
                Destroy(col.gameObject);
                GameManager.CollectedDot();
            }
        }
        else if(col.gameObject.tag.Equals("ghost"))
        {
            if(col.gameObject != null)
            {
                Destroy(this.gameObject);
                Time.timeScale = 0.0f;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // set Waypoint.Direction.
        if (Input.GetKey(KeyCode.A))
        {
            this.setDirection(Waypoint.Direction.Left);          // left
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.setDirection(Waypoint.Direction.Down);          // down
        }
        if (Input.GetKey(KeyCode.D))
        {
            //Debug.Log("D pressed.");
            this.setDirection(Waypoint.Direction.Right);         // right
        }
        if (Input.GetKey(KeyCode.W))
        {
            this.setDirection(Waypoint.Direction.Up);            // up
        }
        //Debug.Log("Direction: " + Direction);


        // check that we're not at the waypoint yet.
        if (!atWaypoint())
        {
            //Debug.Log("moving " + Direction);
            // move
            switch (Direction)
            {
                case Waypoint.Direction.Left:
                    this.transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), CurrentWaypoint.transform.position.y, transform.position.z);
                    break;
                case Waypoint.Direction.Right:
                    this.transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), CurrentWaypoint.transform.position.y, transform.position.z);
                    break;
                case Waypoint.Direction.Down:
                    this.transform.position = new Vector3(CurrentWaypoint.transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
                    break;
                case Waypoint.Direction.Up:
                    this.transform.position = new Vector3(CurrentWaypoint.transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
                    break;
            }
        }
        else
        {
            if (CurrentWaypoint == leftTeleport && Direction == Waypoint.Direction.Left)
            {
                transform.position = new Vector3(rightTeleport.transform.position.x, rightTeleport.transform.position.y, rightTeleport.transform.position.z);
                CurrentWaypoint = rightTeleport;
                return;
            }
            else if (CurrentWaypoint == rightTeleport && Direction == Waypoint.Direction.Right)
            {
                transform.position = new Vector3(leftTeleport.transform.position.x, leftTeleport.transform.position.y, leftTeleport.transform.position.z);
                CurrentWaypoint = leftTeleport;
                return;
            }

            var nextWaypoint = getClosestWaypoint(CurrentWaypoint, Direction);
            //Debug.Log("closest waypoint: " + nextWaypoint);
            // check if the player should stay at the waypoint or keep moving.
            if (nextWaypoint != null)
            {
                CurrentWaypoint = nextWaypoint;
            }
            else
            {
                //Debug.Log("Staying here");
                transform.position = CurrentWaypoint.transform.position;
            }
        }
        // check 
    }

    void FixedUpdate()
    {
        // update the animation state
        anim.SetInteger("direction", (int)Direction);
    }

    // if possible, sets the player's Direction to the given Waypoint.Direction.
    void setDirection(Waypoint.Direction dir)
    {
        bool canChangeDirection = false;
        foreach (var direction in CurrentWaypoint.PossibleDirections)
        {
            Debug.Log("at waypoint? " + atWaypoint());
            Debug.Log("dir: " + ((int)dir) + " Direction: " + ((int)Direction) + "direction: " + ((int)direction));
            if (nearWaypoint())
            {
                if (direction == dir) canChangeDirection = true;
            }
            else
            {
                if (Math.Abs((int)Direction) == Math.Abs((int)dir)) canChangeDirection = true;
            }

        }

        Debug.Log("can change: " + canChangeDirection);
        // change direction
        if (canChangeDirection)
        {
            this.Direction = dir;
        }
    }

    // returns the distance between two vectors
    double getDistance(Vector3 v1, Vector3 v2, Waypoint.Direction dir)
    {
        switch (dir)
        {
            case Waypoint.Direction.Left:
                return Math.Abs(v2.x - v1.x);
            case Waypoint.Direction.Right:
                return Math.Abs(v2.x - v1.x);
            case Waypoint.Direction.Down:
                return Math.Abs(v2.y - v1.y);
            case Waypoint.Direction.Up:
                return Math.Abs(v2.y - v1.y);
        }
        return 0.0;
    }

    Waypoint getClosestWaypoint(Waypoint current, Waypoint.Direction dir)
    {
        var currentPos = current.transform.position;

        Waypoint closest = null;
        foreach (var waypoint in Waypoints)
        {
            var pos = waypoint.transform.position;
            // determine what axis to look on
            switch (dir)
            {
                case Waypoint.Direction.Left:
                    // if: the waypoint is to the left, the y axis is the same, and its closer than the current closest
                    if (pos.x < currentPos.x && pos.y == currentPos.y && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Left) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Left))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Right) && canGoDirection(current, Waypoint.Direction.Left))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Right) && canGoDirection(current, Waypoint.Direction.Left))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Right:
                    // if: the waypoint is to the right, the y axis is the same, and its closer than the current closest
                    if (pos.x > currentPos.x && pos.y == currentPos.y && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Right) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Right))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Left) && canGoDirection(current, Waypoint.Direction.Right))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Left) && canGoDirection(current, Waypoint.Direction.Right))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Down:
                    // if: the waypoint is down from, the x axis is the same, and its closer than the current closest
                    if (pos.y < currentPos.y && pos.x == currentPos.x && waypoint != current)
                    {
                        if (closest != null)
                        {
                            if (getDistance(currentPos, pos, Waypoint.Direction.Down) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Down))
                            {
                                if (canGoDirection(waypoint, Waypoint.Direction.Up) && canGoDirection(current, Waypoint.Direction.Down))
                                {
                                    closest = waypoint;
                                }
                            }
                        }
                        else
                        {
                            if (canGoDirection(waypoint, Waypoint.Direction.Up) && canGoDirection(current, Waypoint.Direction.Down))
                            {
                                closest = waypoint;
                            }
                        }
                    }
                    break;
                case Waypoint.Direction.Up:
                    // if: the waypoint is down from, the x axis is the same, and its closer than the current closest
                    if (pos.y > currentPos.y && pos.x == currentPos.x && waypoint != current)
                    {
                        //Debug.Log("case UP; x: " + currentPos.x + " y: " + currentPos.y + " x(1): " + pos.x + "y(1): " + pos.y);
                        Debug.Log("fulfills requirements");
                        if (closest != null)
                        {
                            //Debug.Log("closest != null");
                            //Debug.Log("closest dist: " + getDistance(currentPos, pos, Waypoint.Direction.Up));
                            if (getDistance(currentPos, pos, Waypoint.Direction.Up) < getDistance(currentPos, closest.transform.position, Waypoint.Direction.Up))
                            {
                                Debug.Log("closer");
                                if (canGoDirection(waypoint, Waypoint.Direction.Down) && canGoDirection(current, Waypoint.Direction.Up))
                                {
                                    Debug.Log("closer and can go");
                                    closest = waypoint;
                                }
                            }
                            else
                            {
                                Debug.Log("no closer");
                            }
                        }
                        else
                        {
                            Debug.Log("closest == null");
                            if (canGoDirection(waypoint, Waypoint.Direction.Down) && canGoDirection(current, Waypoint.Direction.Up))
                            {
                                Debug.Log("closest null, can go direction");
                                closest = waypoint;
                            }
                        }
                    }
                    break;
            }
        }

        // return the closest waypoint
        return closest;
    }


    // see if a waypoint has a certain direction.
    bool canGoDirection(Waypoint waypoint, Waypoint.Direction direction)
    {
        foreach (var dir in waypoint.PossibleDirections)
        {
            if (dir == direction) return true;
        }
        return false;
    }

    // detect if Pacman is at the current waypoint
    bool atWaypoint()
    {
        switch (Direction)
        {
            case Waypoint.Direction.Left:
                // see if we've passed the x pos of the waypoint
                if (transform.position.x <= CurrentWaypoint.transform.position.x) return true;
                else return false;
            case Waypoint.Direction.Right:
                // see if we've passed the x pos waypoint
                if (transform.position.x >= CurrentWaypoint.transform.position.x) return true;
                else return false;
            case Waypoint.Direction.Down:
                // see if we've passed the x pos of the waypoint
                if (transform.position.y <= CurrentWaypoint.transform.position.y) return true;
                else return false;
            case Waypoint.Direction.Up:
                // see if we've passed the x pos waypoint
                if (transform.position.y >= CurrentWaypoint.transform.position.y) return true;
                else return false;
        }
        return false;
    }
    // detect if the player is within a certain range of the waypoint
    bool nearWaypoint()
    {
        var waypointLoc = CurrentWaypoint.transform.position;
        var playerLoc = transform.position;

        // see if the player is within 0.5 of the waypoint
        switch (Direction)
        {
            case Waypoint.Direction.Down:
            case Waypoint.Direction.Up:
                return (Mathf.Abs(waypointLoc.y - playerLoc.y) <= turnForgiveness);
            case Waypoint.Direction.Left:
            case Waypoint.Direction.Right:
                return (Mathf.Abs(waypointLoc.x - playerLoc.x) <= turnForgiveness);
        }
        return false;
    }
}