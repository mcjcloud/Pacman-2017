﻿using UnityEngine;
using System.Collections;
using System;

public class Ghost : MonoBehaviour {

    public GameObject WaypointParent;
    private Waypoint[] Waypoints;
    public Waypoint.Direction Direction;
    public Waypoint CurrentWaypoint;
    public Waypoint StartSpot;
    public Waypoint leftTeleport;
    public Waypoint rightTeleport;
    public float speed;
    public float Delay;

	// Use this for initialization
	void Start () {
        Waypoints = new Waypoint[WaypointParent.transform.childCount];
        int i = 0;
        foreach (Transform tform in WaypointParent.transform)
        {
            Waypoints[i] = tform.gameObject.GetComponent<Waypoint>();
            i++;
        }
    }
	
	// Update is called once per frame
	void Update () {
        // check that we're not at the waypoint yet.
        if (!atWaypoint())
        {
            //Debug.Log("moving " + Direction);
            // move
            switch (Direction)
            {
                case Waypoint.Direction.Left:
                    this.transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), CurrentWaypoint.transform.position.y, transform.position.z);
                    break;
                case Waypoint.Direction.Right:
                    this.transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), CurrentWaypoint.transform.position.y, transform.position.z);
                    break;
                case Waypoint.Direction.Down:
                    this.transform.position = new Vector3(CurrentWaypoint.transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
                    break;
                case Waypoint.Direction.Up:
                    this.transform.position = new Vector3(CurrentWaypoint.transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
                    break;
            }
        }
        else
        {
            // check teleports
            if (CurrentWaypoint == leftTeleport && Direction == Waypoint.Direction.Left)
            {
                Debug.Log("teleporting to right");
                transform.position = new Vector3(rightTeleport.transform.position.x, rightTeleport.transform.position.y, rightTeleport.transform.position.z);
                CurrentWaypoint = rightTeleport;
                return;
            }
            else if (CurrentWaypoint == rightTeleport && Direction == Waypoint.Direction.Right)
            {
                Debug.Log("teleporting to left");
                transform.position = new Vector3(leftTeleport.transform.position.x, leftTeleport.transform.position.y, leftTeleport.transform.position.z);
                CurrentWaypoint = leftTeleport;
                return;
            }

            Waypoint.Direction[] _dirs = new Waypoint.Direction[CurrentWaypoint.PossibleDirections.Length - 1];
            Waypoint.Direction prev = (Waypoint.Direction) ((int)Direction * -1);
            if (CurrentWaypoint.PossibleDirections.Length > 1)
            {
                // build array of directions to go from here
                int offset = 0;
                Debug.Log("possible: " + CurrentWaypoint.PossibleDirections.Length + " withoutPrev: " + (CurrentWaypoint.PossibleDirections.Length - 1));
                for (int i = 0; i < CurrentWaypoint.PossibleDirections.Length; i++)
                {
                    var dir = CurrentWaypoint.PossibleDirections[i];
                    Debug.Log("prev: " + prev + " current loop: " + dir);
                    if (dir != prev)
                    {
                        Debug.Log("i: " + i + " offset: " + offset);
                        _dirs[i - offset] = dir;
                    }
                    else
                    {
                        Debug.Log("incrementing offset");
                        offset += 1;
                    }
                }

                // choose one randomly
                System.Random gen = new System.Random();
                int rand = gen.Next(0, _dirs.Length);
                Direction = _dirs[rand];
            }

            var nextWaypoint = Waypoint.getClosestWaypoint(CurrentWaypoint, Direction, Waypoints);
            //Debug.Log("closest waypoint: " + nextWaypoint);
            // check if the player should stay at the waypoint or keep moving.
            if (nextWaypoint != null)
            {
                CurrentWaypoint = nextWaypoint;
            }
            /*
            else
            {
                Waypoint.Direction[] _dirs = new Waypoint.Direction[CurrentWaypoint.PossibleDirections.Length - 1];
                Waypoint.Direction prev = Direction;
                // build array of directions to go from here
                int offset = 0;
                for(int i = 0; i < CurrentWaypoint.PossibleDirections.Length; i++)
                {
                    var dir = CurrentWaypoint.PossibleDirections[i];
                    if (dir != prev)
                    {
                        _dirs[i - offset] = dir;
                    }
                    else
                    {
                        offset += 1;
                    }
                }

                // choose one randomly
                System.Random gen = new System.Random();
                int rand = gen.Next(0, _dirs.Length);
                Direction = _dirs[rand];
            }
            */
        }
    }

    // detect if Pacman is at the current waypoint
    bool atWaypoint()
    {
        switch (Direction)
        {
            case Waypoint.Direction.Left:
                // see if we've passed the x pos of the waypoint
                if (transform.position.x <= CurrentWaypoint.transform.position.x) return true;
                else return false;
            case Waypoint.Direction.Right:
                // see if we've passed the x pos waypoint
                if (transform.position.x >= CurrentWaypoint.transform.position.x) return true;
                else return false;
            case Waypoint.Direction.Down:
                // see if we've passed the x pos of the waypoint
                if (transform.position.y <= CurrentWaypoint.transform.position.y) return true;
                else return false;
            case Waypoint.Direction.Up:
                // see if we've passed the x pos waypoint
                if (transform.position.y >= CurrentWaypoint.transform.position.y) return true;
                else return false;
        }
        return false;
    }
}
